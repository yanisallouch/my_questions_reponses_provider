import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:questionreponses/data/data.dart';
import 'package:questionreponses/provider/homepage_provider.dart';
import 'package:questionreponses/views/homepage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Questions/Réponses',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: ChangeNotifierProvider(
        create: (_) => HomepageProvider(0, getQuestions()),
        child: HomePage(title: "Questions/Réponses"),
      ),
    );
  }
}
