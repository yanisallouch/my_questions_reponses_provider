import 'package:flutter/material.dart';
import 'package:questionreponses/models/question_model.dart';

class HomepageProvider with ChangeNotifier {
  List<QuestionModel> _questions = [];
  int _index;

  HomepageProvider(this._index, this._questions);

  void nextQuestion() {
    index = (index + 1) % questions.length;
    notifyListeners();
  }

  int get index => _index;

  set index(int value) {
    _index = value;
    notifyListeners();
  }

  List<QuestionModel> get questions => _questions;

  set questions(List<QuestionModel> value) {
    _questions = value;
    notifyListeners();
  }
}
