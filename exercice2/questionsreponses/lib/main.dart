import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:questionsreponses/cubit/homepage_cubit.dart';
import 'package:questionsreponses/data/data.dart';
import 'package:questionsreponses/views/homepage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return BlocProvider(
      create: (_) => HomepageCubit(0, getQuestions()),
      child: BlocBuilder<HomepageCubit, HomepageState>(
        builder: (_, theme) {
          return MaterialApp(
            title: 'Questions/Réponses',
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primarySwatch: Colors.blueGrey,
            ),
            home: Scaffold(
              backgroundColor: Colors.cyan.shade900,
              body: SafeArea(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: HomePage(title: "Questions/Réponses"),
                ),
              ),
            ),
          );
        },
      ));
  }
}
