part of 'homepage_cubit.dart';

@immutable
abstract class HomepageState {}

class HomepageInitial extends HomepageState {
  int index = 0;
  List<QuestionModel> questions = [];

  HomepageInitial(this.index, this.questions);
}
