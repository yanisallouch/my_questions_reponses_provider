import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:questionsreponses/data/data.dart';
import 'package:questionsreponses/models/question_model.dart';

part 'homepage_state.dart';

class HomepageCubit extends Cubit<HomepageState> {
  HomepageCubit(this._index, this._questions) : super(HomepageInitial(0, getQuestions()));

  List<QuestionModel> _questions = [];
  int _index = 0;

  void nextQuestion() {
    index = (index + 1) % questions.length;
    emit(HomepageInitial(index, questions));
  }

  int get index => _index;

  set index(int value) {
    _index = value;
    emit(HomepageInitial(index, questions));
  }

  List<QuestionModel> get questions => _questions;

  set questions(List<QuestionModel> value) {
    _questions = value;
    emit(HomepageInitial(index, questions));
  }
}